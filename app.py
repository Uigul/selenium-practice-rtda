from flask import Flask, render_template

app = Flask(__name__)


@app.route('/')
def hello_world():
    breadcrumb_links = [
        {'url': '/', 'label': 'Main page'}
    ]
    return render_template('index.html', logo='static/logo.png', breadcrumb_links=breadcrumb_links)


@app.route('/locators-intro')
def locators_intro():
    breadcrumb_links = [
        {'url': '/', 'label': 'Main page'},
        {'url': '/locators-intro', 'label': 'Locators - intro'},
    ]
    return render_template('locators/locators_intro.html', logo='static/logo.png', breadcrumb_links=breadcrumb_links)


@app.route('/locators-id')
def locators_id():
    breadcrumb_links = [
        {'url': '/', 'label': 'Main page'},
        {'url': '/locators-intro', 'label': 'Locators - intro'},
        {'url': '/locators-id', 'label': 'ID'},
    ]
    return render_template('locators/id.html', logo='static/logo.png', breadcrumb_links=breadcrumb_links)



@app.route('/locators-name')
def locators_name():
    breadcrumb_links = [
        {'url': '/', 'label': 'Main page'},
        {'url': '/locators-intro', 'label': 'Locators - intro'},
        {'url': '/locators-id', 'label': 'Name'},
    ]
    return render_template('locators/name.html', logo='static/logo.png', breadcrumb_links=breadcrumb_links)


@app.route('/locators-class')
def locators_class():
    breadcrumb_links = [
        {'url': '/', 'label': 'Main page'},
        {'url': '/locators-intro', 'label': 'Locators - intro'},
        {'url': '/locators-id', 'label': 'Class'},
    ]
    return render_template('locators/class.html', logo='static/logo.png', breadcrumb_links=breadcrumb_links)


@app.route('/locators-link-text')
def locators_link_text():
    breadcrumb_links = [
        {'url': '/', 'label': 'Main page'},
        {'url': '/locators-intro', 'label': 'Locators - intro'},
        {'url': '/locators-id', 'label': 'Link text'},
    ]
    return render_template('locators/link_text.html', logo='static/logo.png', breadcrumb_links=breadcrumb_links)


@app.route('/locators-tag-name')
def locators_tag_name():
    breadcrumb_links = [
        {'url': '/', 'label': 'Main page'},
        {'url': '/locators-intro', 'label': 'Locators - intro'},
        {'url': '/locators-id', 'label': 'Tag name'},
    ]
    return render_template('locators/tag_name.html', logo='static/logo.png', breadcrumb_links=breadcrumb_links)


@app.route('/login-form')
def operations_login_form():
    breadcrumb_links = [
        {'url': '/', 'label': 'Main page'},
        {'url': '/#', 'label': 'Operations on WebElements'},
        {'url': '/login-form', 'label': 'Login form'},
    ]
    return render_template('operations/login/login_form.html', logo='static/logo.png', breadcrumb_links=breadcrumb_links)


@app.route('/success-login')
def success_login():
    breadcrumb_links = [
        {'url': '/', 'label': 'Main page'},
        {'url': '/#', 'label': 'Operations on WebElements'},
        {'url': '/login-form', 'label': 'Login form'},
    ]
    return render_template('operations/login/success_login.html', logo='static/logo.png', breadcrumb_links=breadcrumb_links, logged_in_image='static/logged_in.png')


@app.route('/checkers')
def checkers():
    breadcrumb_links = [
        {'url': '/', 'label': 'Main page'},
        {'url': '/#', 'label': 'Operations on WebElements'},
        {'url': '/checkers', 'label': 'Checkers'},
    ]
    return render_template('operations/checkers.html', logo='static/logo.png', breadcrumb_links=breadcrumb_links)


@app.route('/checkboxes-radiobuttons')
def boxes():
    breadcrumb_links = [
        {'url': '/', 'label': 'Main page'},
        {'url': '/#', 'label': 'Operations on WebElements'},
        {'url': '/checkboxes-radiobuttons', 'label': 'Checkboxes and radio buttons'},
    ]
    return render_template('operations/boxes.html', logo='static/logo.png', breadcrumb_links=breadcrumb_links)


@app.route('/random-number')
def random_number():
    breadcrumb_links = [
        {'url': '/', 'label': 'Main page'},
        {'url': '/#', 'label': 'Operations on WebElements TODO'},  # TODO CHANGE IT
        {'url': '/random-number', 'label': 'Random number'},
    ]
    return render_template('advanced/random_number.html', logo='static/logo.png', breadcrumb_links=breadcrumb_links)


@app.route('/simple-actions')
def simple_actions():
    breadcrumb_links = [
        {'url': '/', 'label': 'Main page'},
        {'url': '/simple-actions', 'label': 'Simple actions'}]
    return render_template('class_action/simple_actions.html', logo='static/logo.png', breadcrumb_links=breadcrumb_links, first_image='static/egg.png', second_image='static/crashed_egg.png')


@app.route('/drag-n-drop')
def drag_n_drop():
    breadcrumb_links = [
        {'url': '/', 'label': 'Main page'},
        {'url': '/drag-n-drop', 'label': 'Magazine shop - drag&drop'},
    ]
    return render_template('class_action/drag_n_drop.html', logo='static/logo.png', breadcrumb_links=breadcrumb_links)


@app.route('/context-menu')
def context_menu():
    breadcrumb_links = [
        {'url': '/', 'label': 'Main page'},
        {'url': '/context-menu', 'label': 'Context menu'},
    ]
    return render_template('class_action/context_menu.html', logo='static/logo.png', breadcrumb_links=breadcrumb_links)


@app.route('/slider')
def slider():
    breadcrumb_links = [
        {'url': '/', 'label': 'Main page'},
        {'url': '/slider', 'label': 'Slider'},
    ]
    return render_template('class_action/slider.html', logo='static/logo.png', breadcrumb_links=breadcrumb_links)


if __name__ == '__main__':
    app.run()
